#!/usr/bin/env sh

set -ex

latexmk -lualatex -halt-on-error -shell-escape Ergänzung_Übung4.tex
mv Ergänzung_Übung4.pdf public/
